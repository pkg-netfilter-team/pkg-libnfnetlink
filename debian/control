Source: libnfnetlink
Section: libs
Priority: optional
Maintainer: Debian Netfilter Packaging Team <team+pkg-netfilter-team@tracker.debian.org>
Uploaders: Alexander Wirt <formorer@debian.org>,
           Arturo Borrero Gonzalez <arturo@debian.org>,
           Jeremy Sowden <azazel@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://git.netfilter.org/libnfnetlink
Vcs-Git: https://salsa.debian.org/pkg-netfilter-team/pkg-libnfnetlink.git
Vcs-Browser: https://salsa.debian.org/pkg-netfilter-team/pkg-libnfnetlink

Package: libnfnetlink0
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Netfilter netlink library
 libnfnetlink is the low-level library for netfilter related
 kernel/userspace communication. It provides a generic messaging
 infrastructure for in-kernel netfilter subsystems (such as
 nfnetlink_log, nfnetlink_queue, nfnetlink_conntrack) and their
 respective users and/or management tools in userspace.

Package: libnfnetlink-dev
Section: libdevel
Architecture: linux-any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: libnfnetlink0 (= ${binary:Version}), pkgconf, ${misc:Depends}
Description: Development files for libnfnetlink0
 libnfnetlink is the low-level library for netfilter related
 kernel/userspace communication. It provides a generic messaging
 infrastructure for in-kernel netfilter subsystems (such as
 nfnetlink_log, nfnetlink_queue, nfnetlink_conntrack) and their
 respective users and/or management tools in userspace.
 .
 This package provides development files and static libraries.
