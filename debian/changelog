libnfnetlink (1.0.2-4) UNRELEASED; urgency=medium

  * [cb78ea3] d/control: bump Standards-Version to 4.7.0

 -- Jeremy Sowden <azazel@debian.org>  Fri, 03 May 2024 18:30:12 +0100

libnfnetlink (1.0.2-3) unstable; urgency=medium

  * [e748c39] d/rules: remove export of dpkg build flags
  * [4abd7d2] d/u/signing-key.asc: minimize signing key
  * [afce643] d/control: remove superfluous build-dep on libtool
  * [9d67834] d/control: bump Standards-Version to 4.6.2
  * [d3d3d6d] d/control: update pkg-config dependency to pkgconf
  * [a49a868] d/control: update `Maintainer:` e-mail address
  * [8fc4b62] d/control, d/copyright: update my e-mail address
  * [cc2c08a] d/rules: add support for the `terse` build option

 -- Jeremy Sowden <azazel@debian.org>  Thu, 28 Mar 2024 21:07:48 +0000

libnfnetlink (1.0.2-2) unstable; urgency=medium

  * [1e8f863] d/control: set Rules-Requires-Root: no.
  * [23f21e4] d/control: remove -dev dependency on ${shlibs:Depends}
  * [baea709] d/control: add myself to uploaders
  * [b943106] d/copyright: convert to DEP-5 format
  * [6e7d7c9] d/*: remove remnants of -dbg package
  * [494eae9] d/gbp.conf: buildpackage: set pristine-tar to true
  * [58e2f14] d/gbp.conf: buildpackage: set upstream-signatures to on
  * [c700a01] d/gbp.conf: dch: set id-length to 7
  * [7193039] d/changelog: remove trailing white-space
  * [da4bcfa] d/watch: bump version
  * [99039a0] d/watch: switch to HTTPS
  * [c63ee11] d/libnfnetlink0.symbols: add `Build-Depends-Package` field
  * [abef6eb] d/copyright: add Arturo and me to copyright field for debian/*

 -- Jeremy Sowden <jeremy@azazel.net>  Fri, 08 Apr 2022 14:39:50 +0100

libnfnetlink (1.0.2-1) unstable; urgency=medium

  [ Arturo Borrero Gonzalez ]
  * [6d7df47] debian/: add gitlab-ci.yml file

  [ Michael Biebl ]
  * [48db57e] libnftnetlink: use arch:linux-any (Closes: #745159)

  [ Arturo Borrero Gonzalez ]
  * [efbfa62] libnfnetlink: refresh packaging
  * [737d01d] New upstream version 1.0.2
  * [e5f6b25] d/watch: introduce support for PGP
  * [6331cf2] d/rules: modernize
  * [4ccd263] libnfnetlink-dev: adjust Pre-Depends
  * [cb62ced] libnfnetlink: mark as Priority: optional
  * [a3987b0] libnfnetlink: add symbols file

 -- Arturo Borrero Gonzalez <arturo@debian.org>  Thu, 07 Apr 2022 19:28:41 +0200

libnfnetlink (1.0.1-3) unstable; urgency=medium

  * [54caaa4] Handle symlink to directory conversion in a policy conform way.
    Usage of maintscript helpers needs a predepend on dpkg. (Closes: #736310)

 -- Alexander Wirt <formorer@debian.org>  Tue, 15 Apr 2014 23:44:14 +0200

libnfnetlink (1.0.1-2) unstable; urgency=low

  * [0ea816f] Imported Upstream version 1.0.1
              (Closes: #699204)
  * [ee23dbe] Bump standards version and debhelper version
  * [674fed5] Move to 3.0 for getting bz2 support
  * [8cc4c93] Update maintainer field
  * [e9834ad] Update Vcs-* fields
  * [d4b137c] Play nicer with git
  * [63bbed0] Add misc depends
  * [1ec36bf] wrap-and-sort
  * [cab9e24] Add multiarch stuff
  * [f3dc6bb] convert package to dh7
  * [c54ff15] Bump dh compat to 9 (hardening)
  * [edd28ca] Fix filepattern for package installation

 -- Alexander Wirt <formorer@debian.org>  Sat, 18 May 2013 13:46:25 +0200

libnfnetlink (1.0.0-1.1+libtool) unreleased; urgency=low

  * Use dh-autoreconf to update libtool at build time, in order to fix an x32
    build failure.

 -- Daniel Schepler <schepler@debian.org>  Sun, 27 Jan 2013 15:26:08 -0800

libnfnetlink (1.0.0-1.1) unstable; urgency=low

  [ David Prevot ]
  * Non-maintainer upload.
  * iftable: fix incomplete list of interfaces via nlif_query. RTM_GETLINK
    with NLM_F_DUMP returns a multi-part netlink message. The existing code
    only handled the first message of it, thus, ignoring the remaining
    interfaces. This causes a bug in conntrackd. Report 8b15e48 from upstream.
    (Closes: #684863).

 -- Christian Perrier <bubulle@debian.org>  Tue, 01 Jan 2013 19:19:15 +0100

libnfnetlink (1.0.0-1) unstable; urgency=low

  [ Max Kellermann ]
  * new upstream release (Closes: #545914)

  [ Alexander Wirt ]
  * Bump standards version (no changes)
  * Move debug package to section debug

 -- Alexander Wirt <formorer@debian.org>  Sun, 13 Sep 2009 21:43:53 +0200

libnfnetlink (0.0.41-1) unstable; urgency=low

  [ Max Kellermann ]
  * New upstream release
    - increased shlibs version because the symbol nlif_get_ifflags was
      added
  * Priority "extra"

  [ Alexander Wirt ]
  * bump standards version

 -- Alexander Wirt <formorer@debian.org>  Thu, 02 Apr 2009 10:18:29 +0200

libnfnetlink (0.0.39-1) unstable; urgency=low

  * New upstream release

 -- Alexander Wirt <formorer@debian.org>  Tue, 22 Jul 2008 23:09:10 +0200

libnfnetlink (0.0.38-1) unstable; urgency=low

  [ Max Kellermann ]
  * new upstream release (Closes: #491884)

  [ Alexander Wirt ]
  * Bump standards version (No changes)

 -- Alexander Wirt <formorer@debian.org>  Tue, 22 Jul 2008 22:56:54 +0200

libnfnetlink (0.0.33-1) unstable; urgency=low

  [ Max Kellermann ]
  * new upstream release
  * priority changed to "extra"
  * Standards-Version 3.7.3

 -- Alexander Wirt <formorer@debian.org>  Fri, 21 Mar 2008 22:30:43 +0100

libnfnetlink (0.0.30-2) unstable; urgency=low

  * Build depend on libtool (Closes: #451450)

 -- Alexander Wirt <formorer@debian.org>  Fri, 16 Nov 2007 08:50:07 +0100

libnfnetlink (0.0.30-1) unstable; urgency=low

  [ Max Kellermann ]
  * new upstream release (Closes: #448777)

  [ Alexander Wirt ]
  * Fix distclean call in rules file
  * Move compat from rules file to debian/compat
  * Make package Bin NMU safe

 -- Alexander Wirt <formorer@debian.org>  Mon, 12 Nov 2007 11:33:36 +0100

libnfnetlink (0.0.25-1) unstable; urgency=low

  * new upstream release
  * bumped soversion to 0

 -- Max Kellermann <max@duempel.org>  Tue, 13 Mar 2007 08:17:32 +0100

libnfnetlink (0.0.16-1) unstable; urgency=low

  * initial debian release (Closes: #388613)

 -- Max Kellermann <max@duempel.org>  Thu, 21 Sep 2006 18:04:29 +0200
